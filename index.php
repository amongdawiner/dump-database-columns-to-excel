<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Create a new spreadsheet
$spreadsheet = new Spreadsheet();

// Get the database connection
$pdo = new PDO("mysql:host=127.0.0.1;port=8889;dbname=nbc_alternative_channels", "nbc", "Nbc@2022!");

// Get all table names in the database
$query = $pdo->query("SHOW TABLES");
$tables = $query->fetchAll(PDO::FETCH_COLUMN);

// Loop through each table
foreach ($tables as $table) {
    // Create a new worksheet for each table
    $worksheet = $spreadsheet->createSheet();
    $worksheet->setTitle($table);

    // Get the column details for the table
    $query = $pdo->query("SHOW FULL COLUMNS FROM $table");
    $columns = $query->fetchAll(PDO::FETCH_ASSOC);

    // Set the column headers in the first row
    $headers = array_keys($columns[0]);
    $worksheet->fromArray($headers, null, 'A1');

    // Add the column details to the worksheet
    $data = [];
    foreach ($columns as $column) {
        $data[] = array_values($column);
    }
    $worksheet->fromArray($data, null, 'A2');
}

// Create a new Excel file
$writer = new Xlsx($spreadsheet);
$writer->save('column_details.xlsx');
